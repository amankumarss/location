import UIKit
import CoreData
import CoreLocation
import Foundation
import FirebaseDatabase
import SystemConfiguration

// MARK:- Coordinate Class
class Coordinate {
    
    var latitude: String
    var longitude: String
    
    init(lat: String, long: String) {
        self.latitude = lat
        self.longitude = long
    }
}

struct FirebaseDetails{
    static let path = "Coordinates/"
    static let lat = "latitude"
    static let long = "longitude"
}

// MARK:- View Controller
class ViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var coordinateTableView: UITableView!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet weak var start: UIButton!
    @IBOutlet weak var stop: UIButton!
    @IBOutlet weak var resetData: UIButton!
    
    var coordinateArr = [Coordinate]()
    var manager: CLLocationManager?
    var locationDetails: String?
    var gettingLocation: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationLabel.text = "Ready!"
        
        coordinateTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.getLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        start.isHidden = true
    }
    
    func showCoreDataValue(){
        let result = self.readFromCoreData()
        
        for data in result as! [NSManagedObject] {
            let getLat = data.value(forKey: "latitude") as! Double
            let getLong = data.value(forKey: "longitude") as! Double
            coordinateArr.append(Coordinate(lat: String(getLat), long: String(getLong)))
        }
    }
    
    // MARK:- Location
    
    func getLocation(){
        if gettingLocation == true { return }
        
        manager = CLLocationManager()
        manager?.delegate = self
        manager?.desiredAccuracy = kCLLocationAccuracyBest
        manager?.requestWhenInUseAuthorization()
        manager?.startUpdatingLocation()
        
        gettingLocation = true
    }
    
    func stopUpdatingLocation(){
        if gettingLocation == false { return }
        manager = CLLocationManager()
        manager?.delegate = self
        manager?.desiredAccuracy = kCLLocationAccuracyBest
        manager?.requestWhenInUseAuthorization()
        manager?.stopUpdatingLocation()
        gettingLocation = false
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let mostRecentLocation = locations.last else { return }
        
        let latitude = mostRecentLocation.coordinate.latitude
        let longitude = mostRecentLocation.coordinate.longitude
        
        
        locationDetails = "Latitude: \(latitude) \nLongitude: \(longitude)"
        locationLabel.text = locationDetails
        locationLabel.textAlignment = .left
        
        //        writeInCoreData(latitude, longitude);
        //        self.syncWithFirebase()
        
        var currentLocationIndex:Int = getIntDataFromUserDefault("currentLocationIndexKey")
        var lastLocationIndex:Int = getIntDataFromUserDefault("lastLocationIndexKey")
        
        self.createDataInFirebase(currentLocationIndex, latitude, longitude)
        
        if isConnectedToNetwork(){
            print("---> ConnectedToNetwork")
            self.readFromFirebase(lastLocationIndex, currentLocationIndex)
        }else {
            print("---> NOT Connected")
        }
        
        coordinateTableView.reloadData()
        
        currentLocationIndex = currentLocationIndex + 1
        UserDefaults.standard.setValue(currentLocationIndex, forKey: "currentLocationIndexKey")
        
        if isConnectedToNetwork(){
            lastLocationIndex = currentLocationIndex
            UserDefaults.standard.setValue(lastLocationIndex, forKey: "lastLocationIndexKey")
        }
        UserDefaults.standard.synchronize()
    }
    
    // MARK:- UserDefault Operation
    func getIntDataFromUserDefault(_ key: String) -> Int{
        var keyValue:Int = 1
        if UserDefaults.standard.integer(forKey: key) != 0 { keyValue = UserDefaults.standard.integer(forKey: key)}
        return keyValue
    }
    
    func setIntDataInUserDefault(_ keyValue: Int, _ key: String){
        UserDefaults.standard.set(keyValue, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    // MARK:- Core Data Operation
    func createInCoreData(_ latitude: Double, _ longitude: Double){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let locationEntity = NSEntityDescription.entity(forEntityName: "Coordinates", in: managedContext)!
        let details = NSManagedObject(entity: locationEntity, insertInto: managedContext)
        
        details.setValue(latitude, forKeyPath: "latitude")
        details.setValue(longitude, forKeyPath: "longitude")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func readFromCoreData() -> [Any]{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return [] }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Coordinates")
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            return result.reversed()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return []
    }
    
    func deleteDataFromCoreData(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Coordinates")
        
        do{
            let test = try managedContext.fetch(fetchRequest)
            let objectToDelete = test[0] as! NSManagedObject
            managedContext.delete(objectToDelete)
            do{
                try managedContext.save()
                print("DATA CLEARED")
            }
            catch let error as NSError {
                print("Could not delete. \(error), \(error.userInfo)")
            }
        }
        catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
        }
    }
    
    // MARK:- Firebase Operation
    
    func syncWithFirebase(){
        let result = readFromCoreData()
        
        for data in result as! [NSManagedObject] {
            let getLatitude = data.value(forKey: "latitude") as! Double
            let getLongitude = data.value(forKey: "longitude") as! Double
            
            var ref: DatabaseReference!
            ref = Database.database().reference()
            
            ref.child("\(result.count)").setValue(["latitude": getLatitude, "longitude": getLongitude])
            break
        }
    }
    
    func createDataInFirebase(_ currentLocationIndex: Int, _ latitude: Double, _ longitude:Double){
        var ref: DatabaseReference!
        ref = Database.database().reference()
        ref.child("\(FirebaseDetails.path)\(currentLocationIndex)").setValue([FirebaseDetails.lat: String(latitude), FirebaseDetails.long: String(longitude)])
    }
    
    func readFromFirebase(_ lastLocationIndex:Int, _ currentLocationIndex: Int){
        for i in lastLocationIndex...currentLocationIndex{
            var ref: DatabaseReference!
            ref = Database.database().reference()
            ref.child("\(FirebaseDetails.path)\(i)").observe(.value, with: { snapshot in
                let coordinatesObj = snapshot.value as? NSDictionary
                let lat = coordinatesObj?[FirebaseDetails.lat] as? String ?? "00.00"
                let long = coordinatesObj?[FirebaseDetails.long] as? String ?? "00.00"
                
                print("##### Fetch Successfully! \(i): latitude: \(lat) longitude: \(long)")
                self.coordinateArr.append(Coordinate(lat: String(lat), long: String(long)))
            }) { error in print(error.localizedDescription) }
        }
        
        UserDefaults.standard.setValue(currentLocationIndex, forKey: "lastLocationIndexKey")
        UserDefaults.standard.synchronize()
    }
    
    func deleteDataInFirebase(_ name: String){
        var ref: DatabaseReference!
        ref = Database.database().reference()
        ref.child(name).removeValue()
    }
    
    // MARK:- Start/Stop/Reset
    
    func onClickStart(){
        self.getLocation()
        start.isHidden = true
        stop.isHidden = false
        
        print("STARTED!!")
    }
    
    func onClickStop(){
        self.stopUpdatingLocation()
        locationLabel.text = "STOPPED!!"
        locationLabel.textAlignment = .center
        start.isHidden = false
        stop.isHidden = true
        
        print("STOPPED!!")
    }
    
    func onClickReset(){
        //        self.deleteInCoreData()
        self.coordinateArr.removeAll()
        coordinateTableView.reloadData()
        self.deleteDataInFirebase("location")
    
        UserDefaults.standard.setValue(1, forKey: "currentLocationIndexKey")
        UserDefaults.standard.setValue(1, forKey: "lastLocationIndexKey")
        UserDefaults.standard.synchronize()
        
        print(getIntDataFromUserDefault("lastLocationIndexKey"))
        print(getIntDataFromUserDefault("currentLocationIndexKey"))
        
        print("DATA CLEARED")
    }
    
    // MARK:- ActionButton
    
    @IBAction func startActionBtn(_ sender: UIButton) {
        self.onClickStart()
    }
    
    @IBAction func stopActionBtn(_ sender: UIButton) {
        self.onClickStop()
    }
    
    @IBAction func resetData(_ sender: UIButton) {
        self.onClickStop()
        self.onClickReset()
    }
    
    // MARK:- Network Check
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        if flags.isEmpty {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
}

// MARK: - UITableViewDataSource

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coordinateArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = "\(indexPath.row + 1). Lat: \(coordinateArr[indexPath.row].latitude) Long: \(coordinateArr[indexPath.row].longitude)"
        return cell
    }
}
